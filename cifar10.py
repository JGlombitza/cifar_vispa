import numpy as np
import matplotlib.pyplot as plt

try:
    # python 2
    import cPickle as pickle
    pickle_kwargs = {}
except:
    # python 3
    import pickle
    pickle_kwargs = {'encoding': 'latin1'}


labels = np.array([
    'airplane',
    'automobile',
    'bird',
    'cat',
    'deer',
    'dog',
    'frog',
    'horse',
    'ship',
    'truck'])


def load_data():
    """
    Load CIFAR 10 data from pickles, returning
    X: images, shape = (60000, 32, 32, 3)
    Y: labels (0-9), shape = (60000)
    """
    X = np.empty((6*10000, 3*32*32))  # images
    Y = np.empty((6*10000))  # labels

    # loop over files
    for i in range(6):
        fname = '/net/scratch/deeplearning/CIFAR/cifar-10-data/batch_%i.pickle' % (i+1)
        with open(fname, mode='rb') as file:
            p = pickle.load(file, **pickle_kwargs)
            i0, i1 = i*10000, (i+1)*10000  # start and end index
            X[i0:i1] = p['data']
            Y[i0:i1] = p['labels']

    # reshape image data (N, 3*32*32) --> (N, 32, 32, 3)
    X = X.reshape(-1, 3, 32, 32).transpose(0, 2, 3, 1)
    return X, Y


def plot_examples(X, Y, n=10, fname=False):
    """ Plot the first n examples for each of the 10 classes in the CIFAR dataset X, Y """
    fig, axes = plt.subplots(n, 10, figsize=(10, n))
    for l in range(10):
        axes[0, l].set_title(labels[l], fontsize='smaller')
        m = Y == l  # boolean mask: True for all images of label l
        for i in range(n):
            image = X[m][i].astype('uint8')  # imshow expects uint8
            ax = axes[i, l]
            ax.imshow(image, origin='upper')
            ax.set(xticks=[], yticks=[])
    return figsave(fig, fname)


def plot_prediction(X, Y, Y_predict, fname=False):
    """
    Plot image X along with predicted probabilities Y_predict.
    X: CIFAR image, shape = (32, 32, 3)
    Y: CIFAR label, one-hot encoded, shape = (10)
    Y_predict: predicted probabilities, shape = (10)
    """
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))

    # plot image
    ax1.imshow(X.astype('uint8'), origin='upper')
    ax1.set(xticks=[], yticks=[])

    # plot probabilities
    ax2.barh(np.arange(10), Y_predict, align='center')
    ax2.set(xlim=(0, 1), xlabel='Score', yticks=[])
    for i in range(10):
        c = 'red' if (i == np.argmax(Y)) else 'black'
        ax2.text(0.05, i, labels[i].capitalize(), ha='left', va='center', color=c)
    return figsave(fig, fname)


def plot_confusion(Y_true, Y_predict, fname=False):
    """
    Plot confusion matrix
    Y_true:    array of true classifications (0-9), shape = (N)
    Y_predict: array of predicted classifications (0-9), shape = (N)
    """
    C = np.histogram2d(Y_true, Y_predict, bins=np.linspace(-0.5, 9.5, 11))[0]
    Cn = C / np.sum(C, axis=1)

    fig = plt.figure()
    plt.imshow(Cn, interpolation='nearest', vmin=0, vmax=1, cmap=plt.cm.YlGnBu)
    plt.colorbar()
    plt.xlabel('prediction')
    plt.ylabel('truth')
    plt.xticks(range(10), labels, rotation='vertical')
    plt.yticks(range(10), labels)
    for x in range(10):
        for y in range(10):
            plt.annotate('%i' % C[x, y], xy=(y, x), ha='center', va='center')
    return figsave(fig, fname)


def figsave(fig, fname):
    if fname:
        fig.savefig(fname, bbox_inches='tight')
        plt.close(fig)
    else:
        return fig


if __name__ == '__main__':
    # load data and plot some examples
    X, Y = load_data()
    fig = plot_examples(X, Y, fname='examples.png')
    plt.show()
