"""
Convolutional network example for the CIFAR-10 classification task.
Run this script with 'pygpu %file' in the code editor or 'pygpu train-CNN.py' in the terminal.
"""
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import cifar10
import tensorflow as tf
from tensorflow import keras
models = keras.models
layers = keras.layers


# ---------------------------------------------------------
# Boilerplate. You can ignore this part.
# ---------------------------------------------------------
try:
    CONDOR_ID = os.environ['CONDOR_ID']
except KeyError:
    sys.exit('Error: Run this script with "pygpu %file"')
tf.logging.set_verbosity(tf.logging.ERROR)

folder = 'train-CNN-%s' % CONDOR_ID  # folder for training results
os.makedirs(folder)


# ---------------------------------------------------------
# Load and preprocess CIFAR-10 data
# ---------------------------------------------------------
X, Y = cifar10.load_data()  # X: images, Y: labels
print('X: images, shape = ', X.shape)
print('Y: labels, shape = ', Y.shape)

# normalize each pixel and color-channel separately across all images
mX = np.mean(X, axis=0)[np.newaxis]  # shape = (1, 32, 32, 3)
sX = np.std(X, axis=0)[np.newaxis]  # shape = (1, 32, 32, 3)
X_normalized = (X - mX) / sX

# convert labels ("0"-"9") to one-hot encodings, "0" = (1, 0, ... 0) and so on
Y_onehot = keras.utils.to_categorical(Y, 10)

# split train, validation and test samples
i0, i1, i2, i3 = 0, 52500, 55000, 60000
X_train, Y_train = X_normalized[i0:i1], Y_onehot[i0:i1]
X_valid, Y_valid = X_normalized[i1:i2], Y_onehot[i1:i2]
X_test,  Y_test = X_normalized[i2:i3], Y_onehot[i2:i3]


# ----------------------------------------------------------
# Define model
# ----------------------------------------------------------

model = models.Sequential([
    layers.Convolution2D(32, kernel_size=(5, 5), strides=(2, 2), activation='relu', input_shape=(32, 32, 3)),
    layers.Dropout(0.3),
    layers.Convolution2D(64, kernel_size=(5, 5), strides=(2, 2), activation='relu'),
    layers.Dropout(0.3),
    layers.Convolution2D(128, kernel_size=(5, 5), strides=(2, 2), activation='relu'),
    layers.Dropout(0.3),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dropout(0.3),
    layers.Dense(10, activation='softmax')
    ])

print(model.summary())


# ----------------------------------------------------------
# Training
# ----------------------------------------------------------
model.compile(
    loss='categorical_crossentropy',
    optimizer=keras.optimizers.Adam(lr=1E-3),
    metrics=['accuracy'])

model.fit(
    X_train, Y_train,
    batch_size=32,
    epochs=20,
    verbose=2,
    validation_data=(X_valid, Y_valid),
    callbacks=[keras.callbacks.CSVLogger(folder + '/history.csv')])

model.save(folder + '/model.h5')  # save trained network

print('Model performance (loss, accuracy)')
print('Train: %.4f, %.4f' % tuple(model.evaluate(X_train, Y_train, verbose=0, batch_size=128)))
print('Valid: %.4f, %.4f' % tuple(model.evaluate(X_valid, Y_valid, verbose=0, batch_size=128)))
print('Test:  %.4f, %.4f' % tuple(model.evaluate(X_test,  Y_test,  verbose=0, batch_size=128)))


# ----------------------------------------------------------
# Plots
# ----------------------------------------------------------
# training curves
history = np.genfromtxt(folder+'/history.csv', delimiter=',', names=True)

fig, ax = plt.subplots(1)
ax.plot(history['epoch'], history['loss'],     label='training')
ax.plot(history['epoch'], history['val_loss'], label='validation')
ax.legend()
ax.set(xlabel='epoch', ylabel='loss')
fig.savefig(folder+'/loss.png')

fig, ax = plt.subplots(1)
ax.plot(history['epoch'], history['acc'],     label='training')
ax.plot(history['epoch'], history['val_acc'], label='validation')
ax.legend()
ax.set(xlabel='epoch', ylabel='accuracy')
fig.savefig(folder+'/accuracy.png')


# calculate predictions for test set
Y_predict = model.predict(X_test, batch_size=128)

# convert back to class labels (0-9)
Y_predict_cl = np.argmax(Y_predict, axis=1)
Y_test_cl = np.argmax(Y_test, axis=1)

# compare prediction and truth
m = Y_predict_cl == Y_test_cl
i0 = np.arange(5000)[~m]  # misclassified images
i1 = np.arange(5000)[m]  # correctly classified images

# original (unnormalized) test images
X_test = X[i2:i3]

# plot first 10 false classifications
fname = folder + '/false_%i.png'
for i in i0[0:10]:
    cifar10.plot_prediction(X_test[i], Y_test[i], Y_predict[i], fname=fname % i)

# plot first 10 correct classifications
fname = folder + '/correct_%i.png'
for i in i1[0:10]:
    cifar10.plot_prediction(X_test[i], Y_test[i], Y_predict[i], fname=fname % i)

# plot confusion matrix
cifar10.plot_confusion(Y_test_cl, Y_predict_cl, fname=folder + '/confusion.png')
