"""
Average over several trained models for the CIFAR-10 task and evaluate the ensemble prediction.
Run this script with 'pygpu' or 'pycpu' (no GPU required).
"""

import numpy as np
import os
import sys
import cifar10
import keras
import tensorflow as tf


# ---------------------------------------------------------
# Boilerplate. You can ignore this part.
# ---------------------------------------------------------
try:
    CONDOR_ID = os.environ['CONDOR_ID']
except KeyError:
    sys.exit('Error: Run this script with "pygpu %file"')

# limit available GPU memory to allow for 2 jobs per GPU, please do not change!
config = tf.ConfigProto(gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.45))
keras.backend.tensorflow_backend.set_session(tf.Session(config=config))
# ---------------------------------------------------------


# ---------------------------------------------------------
# Load and preprocess CIFAR-10 data
# ---------------------------------------------------------
X, Y = cifar10.load_data()  # X: images, Y: labels
print('X: images, shape = ', X.shape)
print('Y: labels, shape = ', Y.shape)

# normalize each pixel and color-channel separately across all images
mX = np.mean(X, axis=0)[np.newaxis]  # shape = (1, 32, 32, 3)
sX = np.std(X, axis=0)[np.newaxis]  # shape = (1, 32, 32, 3)
X_normalized = (X - mX) / sX

# convert labels ("0"-"9") to one-hot encodings, "0" = (1, 0, ... 0) and so on
Y_onehot = keras.utils.np_utils.to_categorical(Y, 10)

# split train, validation and test samples
i0, i1, i2, i3 = 0, 52500, 55000, 60000
X_train, Y_train = X_normalized[i0:i1], Y_onehot[i0:i1]
X_valid, Y_valid = X_normalized[i1:i2], Y_onehot[i1:i2]
X_test,  Y_test = X_normalized[i2:i3], Y_onehot[i2:i3]


# ----------------------------------------------------------
# Load and evaluate some pre-trained models
# ----------------------------------------------------------

# path to some pre-trained models, you can use your own models here
files = [
    '/net/scratch/deeplearning/CIFAR/models-DCNN/model-1.h5',
    '/net/scratch/deeplearning/CIFAR/models-DCNN/model-2.h5',
    '/net/scratch/deeplearning/CIFAR/models-DCNN/model-3.h5',
    '/net/scratch/deeplearning/CIFAR/models-DCNN/model-4.h5',
    '/net/scratch/deeplearning/CIFAR/models-DCNN/model-5.h5',
    ]

print('Test performance (loss, accuracy)')
Y_ensemble = np.zeros_like(Y_test)

for file in files:
    model = keras.models.load_model(file)
    Y_ensemble += model.predict(X_test)
    print('%.3f, %.3f' % tuple(model.evaluate(X_test,  Y_test,  verbose=0, batch_size=128)))


# convert back to class labels (0-9)
Y_ensemble_cl = np.argmax(Y_ensemble, axis=1)
Y_test_cl = np.argmax(Y_test, axis=1)

# compare prediction and truth
m = Y_ensemble_cl == Y_test_cl
print('Ensemble average: test accuracy = %.3f' % (sum(m)/float(len(m))))

# plot confusion matrix
cifar10.plot_confusion(Y_test_cl, Y_ensemble_cl, fname='confusion.png')
